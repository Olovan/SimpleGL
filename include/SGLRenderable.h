#ifndef SGLRENDERABLE_H
#define SGLRENDERABLE_H


class SGLRenderable
{
    public:
        SGLRenderable();
        virtual ~SGLRenderable();

        virtual void draw();

    protected:
    private:
};

#endif // SGLRENDERABLE_H
