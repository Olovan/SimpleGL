# SimpleGL
Simple Implementation of OpenGL

###Dependencies
- **GLFW** 
- **GLEW** 

**Todo List**
- Set up some 3D rendering
- Find a way to batch multiple differet types of objects into 1 draw call efficiently
- Increase ease of use
- Set up Textures
- Set up Lighting system
